public class Runner {
    public static void main(String[] args) {
        Fisier fisier = new Fisier("articol.txt");

        Cititor cititor1 = new Cititor(fisier);
        cititor1.setName("Cititor1");
        Cititor cititor2 = new Cititor(fisier);
        cititor2.setName("Cititor2");

        String[] linii1 = new String[3];
        linii1[0] = "Linia 1";
        linii1[1] = "Linia 2";
        linii1[2] = "Linia 3";

        String[] linii2 = new String[3];
        linii2[0] = "Linia 4";
        linii2[1] = "Linia 5";
        linii2[2] = "Linia 6";

        Scriitor scriitor1 = new Scriitor(fisier, linii1);
        scriitor1.setName("Scriitor1");
        Scriitor scriitor2 = new Scriitor(fisier, linii2);
        scriitor2.setName("Scriitor2");

        scriitor1.start();
        cititor1.start();
        scriitor2.start();
        cititor2.start();
    }
}
