public class Scriitor extends Thread {
    private Fisier fisier;
    private String[] linii;

    public Scriitor(Fisier fisier, String[] linii) {
        this.fisier = fisier;
        this.linii = linii;
    }

    public void run(){
        for (String linie : linii) {
            fisier.writeToFile(linie);
        }
    }
}
