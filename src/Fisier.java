import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Fisier {

    private String numeFisier;

    public Fisier(String numeFisier) {
        this.numeFisier = numeFisier;
    }

    public synchronized void writeToFile(String s) {
        try {
            FileWriter scriitor = new FileWriter(numeFisier, true);
            System.out.println("Thread " + Thread.currentThread().getName() + " is writing to file");
            scriitor.write(s);
            scriitor.write("\n");
            scriitor.close();
            System.out.println("Thread " + Thread.currentThread().getName() + " finished writing to file");
            Thread.sleep(1000);
        }
        catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    public void readFromFile() {
        try {
            File fisier = new File(numeFisier);
            Scanner cititor = new Scanner(fisier);
            System.out.println("Thread " + Thread.currentThread().getName() + " is reading file contents:");
            while (cititor.hasNextLine()) {
                String linie = cititor.nextLine();
                //System.out.println(linie);
            }
            System.out.println("Thread " + Thread.currentThread().getName() + " finished reading file");
            Thread.sleep(2000);
        }
        catch (FileNotFoundException | InterruptedException exception) {
            exception.printStackTrace();
        }


    }
}
